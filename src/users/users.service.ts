import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './user.entity'
import { AccountsService } from './accounts/accounts.service';
import { Account } from './accounts/account.entity';
// export type User = any;

@Injectable()
export class UsersService {
  private readonly users: User[];

  constructor(
    @InjectRepository(User)private usersRepository: Repository<User>, 
    private accountService: AccountsService,
  ) {
  }
  
  async findOne(username: string): Promise<User> {
    return await this.usersRepository.findOne(username);
  }
}